/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Screenshot Module
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include "screenshot.h"

//======================================================================
// screenshot module initialization
struct screenshot *screenshot_init(struct screenshot *screenshot, int rt) {
  if (! screenshot) { return 0; }
  screenshot->width = 0;
  screenshot->height = 0;
  screenshot->buffer = 0;
  screenshot->rt = rt;
  return screenshot;
}

//======================================================================
// screenshot module display callback
void screenshot_display(struct screenshot *screenshot) {
  if (! screenshot->rt) {
    glReadPixels(0, 0, screenshot->width, screenshot->height, GL_RED, GL_UNSIGNED_BYTE, screenshot->buffer);
    fprintf(stdout, "P5\n%d %d 255\n", screenshot->width, screenshot->height);
    for (int y = screenshot->height - 1; y >= 0; --y) {
      fwrite(screenshot->buffer + y * screenshot->width, screenshot->width, 1, stdout);
    }
  }
}

//======================================================================
// screenshot module reshape callback
void screenshot_reshape(struct screenshot *screenshot, int w, int h) {
  screenshot->width = w;
  screenshot->height = h;
  if (screenshot->buffer) free(screenshot->buffer);
  screenshot->buffer = malloc(w * h);
}

// EOF
