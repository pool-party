/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Main Program Entry Point
===================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "party.h"

void keycb(GLFWwindow* window, int key, int scancode, int action, int mods) {
  (void) window;
  (void) scancode;
  (void) action;
  (void) mods;
  party_keynormal(key, 0, 0);
}

int main(int argc, char **argv) {
  (void) argv;
  /* initialize GLFW etc */
  fprintf(stderr, "pool-party (GPL) 2016 Claude Heiland-Allen <claude@mathr.co.uk>\n");
  fprintf(stderr, "based on:\n");
  fprintf(stderr, "rdex (GPL) 2008,2009,2010 Claude Heiland-Allen <claude@mathr.co.uk>\n");
  srand(time(NULL));
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(1920, 1080, "pool-party", 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  glfwSwapInterval(2);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
  glfwSetKeyCallback(window, keycb);
  /* activate callbacks and enter main loop */
  int rt = argc == 1;
  if (party_init(rt)) {
    party_reshape(1920, 1080);
    while (! glfwWindowShouldClose(window)) {
      glfwPollEvents();
      party_display(window);
      party_idle();
    }
    return 0; // never reached
  } else {
    return 1;
  }
}

// EOF
