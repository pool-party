/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Refraction Shader
===================================================================== */

#ifndef POOL_H
#define POOL_H 1

#include "shader.h"

//======================================================================
// data
struct pool { struct shader shader;
  struct { GLint  texSurface, texCaustics; } uniform;
  struct { int    texSurface, texCaustics; } value;
  GLuint vao;
  int width;
  int height;
};

//======================================================================
// protoypes
struct pool *pool_init(struct pool *pool);
void pool_reshape(
  struct pool *pool, int w, int h
);
void pool_display(
  struct pool *pool, GLuint texSurface, GLuint texCaustics
);

#endif
