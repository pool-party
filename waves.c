/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Wave Simulation
===================================================================== */

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "waves.h"

//======================================================================
// initialization
struct waves *waves_init(struct waves *waves) {
  if (! waves) { return 0; }
  waves->width = 0;
  waves->height = 0;
  waves->hasplan = 0;
  glGenTextures(1, &waves->texture);
  glBindTexture(GL_TEXTURE_2D, waves->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  return waves;
}


double _Complex grand2(void) {
  double u, v, s = 100;
  while (s > 1) {
    u = rand() / (double) RAND_MAX;
    v = rand() / (double) RAND_MAX;
    s = u * u + v * v;
  }
  double t = sqrt(-2 * log(s) / s);
  return u * t + I * v * t;
}

//======================================================================
// reshape callback
void waves_reshape(
  struct waves *waves, int w, int h
) {
  waves->width  = roundtwo(w) / 4;
  waves->height = roundtwo(h) / 4;
  if (waves->hasplan) {
    fftwf_destroy_plan(waves->plan);
    fftwf_free(waves->inbuf);
    fftwf_free(waves->outbuf);
    free(waves->texbuf);
    free(waves->h00);
    free(waves->h0);
    free(waves->w);
    free(waves->wp);
  }
  waves->h00 = malloc(waves->width * waves->height * sizeof(waves->h00[0]));
  waves->h0 = malloc(waves->width * waves->height * sizeof(waves->h0[0]));
  waves->w = malloc(waves->width * waves->height * sizeof(waves->w[0]));
  waves->wp = malloc(waves->width * waves->height * sizeof(waves->wp[0]));
  waves->texbuf = calloc(1, waves->width * waves->height * sizeof(waves->texbuf[0]));
  waves->inbuf = fftwf_malloc(waves->width * waves->height * sizeof(waves->inbuf[0]));
  waves->outbuf = fftwf_malloc(waves->width * waves->height * sizeof(waves->outbuf[0]));
  waves->plan = fftwf_plan_dft_2d(waves->width, waves->height, waves->inbuf, waves->outbuf, FFTW_BACKWARD, FFTW_MEASURE);
  waves->hasplan = 1;
  for (int i = 0; i < waves->width; ++i) {
    for (int j = 0; j < waves->height; ++j) {
      int ix = i * waves->height + j;
      int ii = i >= waves->width / 2 ? i - waves->width : i;
      int jj = j >= waves->height / 2 ? j - waves->height : j;
      float a = 256;
      float g = 3;
      float d = 10;
      float wx = 64;
      float wy = 0;
      float w2 = wx * wx + wy * wy;
      float k2 = ii * ii + jj * jj;
      float dot = fmax(0, (ii * wx + jj * wy) / (sqrtf(k2) * sqrtf(w2)));
      float l = w2 / g;
      float l2 = l * l;
      waves->h00[ix] = grand2();
      waves->h0[ix] = k2 == 0 ? 0 : 1.0 / sqrtf(2) * sqrtf(expf(-k2 * 0.000001 * l2) * a * expf(-1 / (k2 * l2)) / (k2 * k2) * dot * dot);
      waves->w[ix] = sqrtf(g * sqrtf(k2) * tanhf(d * sqrtf(k2)));
      waves->wp[ix] = 6.283185307179586 * (rand() / (double) RAND_MAX);
    }
  }
  glBindTexture(GL_TEXTURE_2D, waves->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, waves->width, waves->height, 0, GL_RED, GL_FLOAT, waves->texbuf);
}

//======================================================================
// wind callback
void waves_adjust(
  struct waves *waves, double wx, double wy
) {
  #pragma omp parallel for
  for (int i = 0; i < waves->width; ++i) {
    for (int j = 0; j < waves->height; ++j) {
      int ix = i * waves->height + j;
      int ii = i >= waves->width / 2 ? i - waves->width : i;
      int jj = j >= waves->height / 2 ? j - waves->height : j;
      float a = 256;
      float g = 3;
      float w2 = wx * wx + wy * wy;
      float k2 = ii * ii + jj * jj;
      float dot = fmaxf(0, (ii * wx + jj * wy) / (sqrtf(k2) * sqrtf(w2)));
      float l = w2 / g;
      float l2 = l * l;
      waves->h0[ix] = k2 == 0 ? 0 : 1.0 / sqrtf(2) * sqrtf(expf(-k2 * 0.000001 * l2) * a * expf(-1 / (k2 * l2)) / (k2 * k2) * dot * dot);
    }
  }
}


//======================================================================
// display callback
void waves_display(
  struct waves *waves
) {
  double t = waves->frame / 30.0;
  waves_adjust(waves, -pow(0.5, 1.333 * (1 - cos(6.283185307179586 * t / 300))) * 48, 0);
  #pragma omp parallel for
  for (int i = 0; i < waves->width; ++i) {
    for (int j = 0; j < waves->height; ++j) {
      int k = i * waves->height + j;
      int nk = ((waves->width - i) % waves->width) * waves->height + ((waves->height - j) % waves->height);
      waves->inbuf[k] = waves->h00[k] * waves->h0[k] * cexpf(I * (waves->w[k] * t + waves->wp[k]))
                + conjf(waves->h00[nk]) * waves->h0[nk] * cexpf(-I * (waves->w[k] * t + waves->wp[k]));
    }
  }
  fftwf_execute(waves->plan);
  for (int k = 0; k < waves->width * waves->height; ++k) {
    waves->texbuf[k] = crealf(waves->outbuf[k]);
  }
  glBindTexture(GL_TEXTURE_2D, waves->texture);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, waves->width, waves->height, GL_RED, GL_FLOAT, waves->texbuf);
  glGenerateMipmap(GL_TEXTURE_2D);
  waves->frame++;
}

//======================================================================
// audio callback
void waves_audio(struct waves *waves, float *left, float *right, float *center) {
  memcpy(left,  &waves->texbuf[15 * waves->height / 32 * waves->width], waves->width * sizeof(float));
  memcpy(right, &waves->texbuf[17 * waves->height / 32 * waves->width], waves->width * sizeof(float));
  center[0] = waves->texbuf[waves->height / 2 * waves->width + 15 * waves->width / 32];
  center[1] = waves->texbuf[waves->height / 2 * waves->width + 17 * waves->width / 32];
}

// EOF
