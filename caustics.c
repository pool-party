/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Caustics Shader
===================================================================== */

#include <stdlib.h>
#include "util.h"
#include "caustics.h"
#include "caustics.vert.c"
#include "caustics.frag.c"

//======================================================================
// initialization
struct caustics *caustics_init(struct caustics *caustics) {
  if (! caustics) { return 0; }
  if (! shader_init(&caustics->shader, caustics_vert, caustics_frag)) {
    return 0;
  }
  shader_uniform(caustics, tex);
  caustics->value.tex = 0;
  caustics->width = 0;
  caustics->height = 0;
  glGenTextures(1, &(caustics->texture));
  glBindTexture(GL_TEXTURE_2D, caustics->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glGenVertexArrays(1, &caustics->vao);
  return caustics;
}

//======================================================================
// reshape callback
void caustics_reshape(
  struct caustics *caustics, int w, int h
) {
  caustics->width  = roundtwo(w) * 2;
  caustics->height = roundtwo(h) * 2;
  float *zero = calloc(1, sizeof(float) * caustics->width * caustics->height);
  glBindTexture(GL_TEXTURE_2D, caustics->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F,
    caustics->width, caustics->height,
    0, GL_RED, GL_FLOAT, zero
  );
  glBindTexture(GL_TEXTURE_2D, 0);
  free(zero);
}

//======================================================================
// display callback
void caustics_display(
  struct caustics *caustics, GLuint fbo, GLuint texture
) {
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
    caustics->texture, 0
  );
  glBindTexture(GL_TEXTURE_2D, texture);
  glUseProgram(caustics->shader.program);
  shader_updatei(caustics, tex);
  glViewport(0, 0, caustics->width, caustics->height);
  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glBindVertexArray(caustics->vao);
  glDrawArrays(GL_TRIANGLES, 0, 6 * caustics->width/2 * caustics->height/2);
  glBindVertexArray(0);
  glDisable(GL_BLEND);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glUseProgram(0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

// EOF
