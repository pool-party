/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Utility Functions
===================================================================== */

#include <assert.h>
#include "util.h"

//======================================================================
// round 'x' up to the nearest power of two (which may be 'x' itself)
unsigned int roundtwo(unsigned int x) {
  assert(x <= 1u << 31); // termination condition
  unsigned int y = 1;
  while (y < x) y <<= 1;
  return y;
}

//======================================================================
// find log2 of the nearest power of two above 'x'
unsigned int logtwo(unsigned int x) {
  assert(x <= 1u << 31); // termination condition
  unsigned int y = 1, z = 0;
  while (y < x) { y <<= 1; z += 1; };
  return z;
}

// EOF
