/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Cubic Interpolation Shader
===================================================================== */

#include "util.h"
#include "cubic.h"
#include "cubic.vert.c"
#include "cubic.frag.c"

//======================================================================
// shader initialization
struct cubic *cubic_init(struct cubic *cubic) {
  if (! cubic) { return 0; }
  if (! shader_init(&cubic->shader, cubic_vert, cubic_frag)) {
    return 0;
  }
  shader_uniform(cubic, tex);
  shader_uniform(cubic, size);
  shader_uniform(cubic, delta);
  cubic->value.tex = 0;
  cubic->value.size[0] = 0;
  cubic->value.size[1] = 0;
  cubic->value.delta[0] = 0;
  cubic->value.delta[1] = 0;
  cubic->width = 0;
  cubic->height = 0;
  glGenTextures(2, cubic->textures);
  glBindTexture(GL_TEXTURE_2D, cubic->textures[0]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, cubic->textures[1]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glGenVertexArrays(1, &cubic->vao);
  return cubic;
}

//======================================================================
// shader reshape callback
void cubic_reshape(
  struct cubic *cubic, int w, int h
) {
  cubic->width  = roundtwo(w);
  cubic->height = roundtwo(h);
  glBindTexture(GL_TEXTURE_2D, cubic->textures[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F,
    cubic->width, cubic->height / 4,
    0, GL_RED, GL_FLOAT, 0
  );
  glBindTexture(GL_TEXTURE_2D, cubic->textures[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F,
    cubic->width, cubic->height,
    0, GL_RED, GL_FLOAT, 0
  );
}

//======================================================================
// shader display callback
void cubic_display(
  struct cubic *cubic, GLuint fbo, GLuint texture
) {
  const int aa = 4;
  glBindVertexArray(cubic->vao);
  glBindTexture(GL_TEXTURE_2D, texture);
  glUseProgram(cubic->shader.program);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
    cubic->textures[0], 0
  );
  cubic->value.size[0] = cubic->width / aa;
  cubic->value.size[1] = cubic->height / aa;
  cubic->value.delta[0] = 1;
  cubic->value.delta[1] = 0;
  shader_updatei(cubic, tex);
  shader_update2i(cubic, size);
  shader_update2i(cubic, delta);
  glViewport(0, 0, cubic->width, cubic->height / aa);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindTexture(GL_TEXTURE_2D, cubic->textures[0]);
  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
    cubic->textures[1], 0
  );
  cubic->value.size[0] = cubic->width;
  cubic->value.size[1] = cubic->height / aa;
  cubic->value.delta[0] = 0;
  cubic->value.delta[1] = 1;
  shader_updatei(cubic, tex);
  shader_update2i(cubic, size);
  shader_update2i(cubic, delta);
  glViewport(0, 0, cubic->width, cubic->height);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glUseProgram(0);
  glBindVertexArray(0);
}

// EOF
