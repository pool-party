/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Main Module
===================================================================== */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "party.h"

//======================================================================
// main module global mutable state
static struct party party;

void party_audiocb(float *l, float *r, int nframes) {
  float g = 0.025;
  for (int i = 0; i < nframes; ++i) {
    float h = i / (float) nframes;
    float h1 = 1 - h;
    float c[2];
    c[0] = h1 * party.center[0] + h * party.center[1];
    c[1] = h1 * party.center[2] + h * party.center[3];
    int j = party.ix / 4;
    float f = (party.ix & 3) / 4.0f;
    float f1 = 1 - f;
    l[i] = tanhf(g * hip(&party.hip[0]
      , h1 * (party.audio[0][j] * f1 + party.audio[0][(j + 1) & 255] * f)
      + h  * (party.audio[2][j] * f1 + party.audio[2][(j + 1) & 255] * f)
      + 5 * sin(0.1 * fmax(c[0], 1) * (h1 * party.audio[4][party.ix] + h * party.audio[6][party.ix])), 5));
    r[i] = tanhf(g * hip(&party.hip[1]
      , h1 * (party.audio[1][j] * f1 + party.audio[1][(j + 1) & 255] * f)
      + h  * (party.audio[3][j] * f1 + party.audio[3][(j + 1) & 255] * f)
      + 5 * sin(0.1 * fmax(c[1], 1) * (h1 * party.audio[5][party.ix] + h * party.audio[7][party.ix])), 5));
    party.ix = (party.ix + 1) & 1023;
  }
  memcpy(&party.audio[0][0], &party.audio[2][0], 256 * sizeof(float));
  memcpy(&party.audio[1][0], &party.audio[3][0], 256 * sizeof(float));
  memcpy(&party.audio[4][0], &party.audio[6][0], 1024 * sizeof(float));
  memcpy(&party.audio[5][0], &party.audio[7][0], 1024 * sizeof(float));
  party.center[0] = party.center[1];
  party.center[2] = party.center[3];
}

static volatile int party_incb = 0;
int party_processcb(jack_nframes_t nframes, void *arg) {
  (void) arg;
  jack_default_audio_sample_t *out[2];
  out[0] = jack_port_get_buffer(party.port[0], nframes);
  out[1] = jack_port_get_buffer(party.port[1], nframes);
  party_incb = 1;
  party_audiocb(out[0], out[1], nframes);
  party_incb = 0;
  return 0;
}

//======================================================================
// main module initialization
int party_init(int rt) {
  memset(&party, 0, sizeof(party));
  party.rt = rt;
  if (! waves_init(&party.waves)) { return 0; }
  { int e = glGetError(); if (e) fprintf(stderr, "GL Error after waves %d\n", e); }
  if (! cubic_init(&party.cubic)) { return 0; }
  { int e = glGetError(); if (e) fprintf(stderr, "GL Error after cubic %d\n", e); }
  if (! caustics_init(&party.caustics)) { return 0; }
  { int e = glGetError(); if (e) fprintf(stderr, "GL Error after caustics %d\n", e); }
  if (! wrapseams_init(&party.wrapseams)) { return 0; }
  { int e = glGetError(); if (e) fprintf(stderr, "GL Error after wrapseams %d\n", e); }
  if (! pool_init(&party.pool)) { return 0; }
  { int e = glGetError(); if (e) fprintf(stderr, "GL Error after pool %d\n", e); }
  if (! screenshot_init(&party.screenshot, rt)) { return 0; }
  { int e = glGetError(); if (e) fprintf(stderr, "GL Error after screenshot %d\n", e); }
  glGenFramebuffers(1, &party.fbo);
  party.starttime = 0;
  party.fullscreen = 0;
  party.done = 0;

  if (rt) {
    if (! (party.client = jack_client_open("pool-party", JackNoStartServer, 0))) {
      fprintf(stderr, "jack server not running?\n");
      exit(1);
    }
    jack_set_process_callback(party.client, party_processcb, 0);
    party.port[0] = jack_port_register(party.client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    party.port[1] = jack_port_register(party.client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (jack_activate(party.client)) {
      fprintf (stderr, "cannot activate JACK client");
      exit(1);
    }
    if (jack_connect(party.client, "pool-party:output_1", "system:playback_1")) {
      fprintf(stderr, "cannot connect output port\n");
    }
    if (jack_connect(party.client, "pool-party:output_2", "system:playback_2")) {
      fprintf(stderr, "cannot connect output port\n");
    }
  } else {
      SF_INFO info = { 0, 48000, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
      party.sndfile = sf_open("pool-party.wav", SFM_WRITE, &info);
  }
  return 1;
}

//======================================================================
// main module reshape callback
void party_reshape(int w, int h) {
  (void) w;
  (void) h;
  waves_reshape(&party.waves, party_tex_size, party_tex_size);
  cubic_reshape(&party.cubic, party_tex_size, party_tex_size);
  caustics_reshape(&party.caustics, party_tex_size, party_tex_size);
  wrapseams_reshape(&party.wrapseams, party_tex_size, party_tex_size);
  pool_reshape(&party.pool, w, h);
  screenshot_reshape(&party.screenshot, w, h);
}

//======================================================================
// main module display callback
void party_display(GLFWwindow *window) {
  waves_display(&party.waves);
  cubic_display(&party.cubic, party.fbo, party.waves.texture);
  caustics_display(&party.caustics, party.fbo, party.cubic.textures[1]);
  wrapseams_display(&party.wrapseams, party.fbo, party.caustics.texture);
  pool_display(&party.pool, party.cubic.textures[1], party.wrapseams.texture);
  if (party.rt)
    while (party_incb)
      ;
  float c[2];
  waves_audio(&party.waves, &party.audio[2][0], &party.audio[3][0], c);
  party.center[1] = c[0];
  party.center[3] = c[1];
  wrapseams_audio(&party.wrapseams, &party.audio[6][0], &party.audio[7][0]);
  if (! party.rt)
  {
    float audio[2][SR/FPS];
    party_audiocb(&audio[0][0], &audio[1][0], SR/FPS);
    float frames[SR/FPS][2];
    for (int i = 0; i < SR/FPS; ++i)
      for (int c = 0; c < 2; ++c)
        frames[i][c] = audio[c][i];
    sf_writef_float(party.sndfile, &frames[0][0], SR/FPS);
  }
  glfwSwapBuffers(window);
  screenshot_display(&party.screenshot);
  party.frame += 1;
  if (! party.rt && party.frame >= FPS * 300) {
    sf_close(party.sndfile);
    exit(0);
  }
}

//======================================================================
// main module exit callback
void party_atexit(void) {
  double timeelapsed = (double) time(NULL) - (double) party.starttime;
  fprintf(stderr, "\n\n--------------------------------------------------------------------------\n");
  fprintf(stderr, "------------------------------- statistics -------------------------------\n");
  fprintf(stderr, "--------------------------------------------------------------------------\n");
  fprintf(stderr, "%10d seconds elapsed (+/- 1)\n", (int) timeelapsed);
  fprintf(stderr, "%10d frames rendered (%f fps)\n", party.frame, party.frame / timeelapsed);
  fprintf(stderr, "%10d frames dropped (+/- 30)\n", (int) timeelapsed * 30 - party.frame);
  fprintf(stderr, "--------------------------------------------------------------------------\n");
}

//======================================================================
// main module idle callback
void party_idle() {
  if (party.starttime == 0) {
    party.starttime = time(NULL);
    party.frame = 0;
    atexit(party_atexit);
  }
  int e = glGetError();
  if (e) {
    fprintf(stderr, "GL err: %d\n", e);
  }
}

//======================================================================
// main module keyboard callback
void party_keynormal(unsigned char key, int x, int y) {
  (void) x;
  (void) y;
  switch (key) {
  case 27: // escape
  case 'Q':
  case 'q':
		exit(0);
    break;
  default:
    break;
  }
}

// EOF
