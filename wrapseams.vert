/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Wrap Seams Shader
===================================================================== */

#version 330 core

out vec2 texCoord;

void main(void) {
  ivec2 d = ivec2(0, 0);
  switch (gl_VertexID % 6) {
    case 0: d = ivec2(0, 0); break;
    case 1: d = ivec2(1, 0); break;
    case 2: d = ivec2(1, 1); break;
    case 3: d = ivec2(1, 1); break;
    case 4: d = ivec2(0, 1); break;
    case 5: d = ivec2(0, 0); break;
  }
  gl_Position = vec4(2.0 * vec2(d) - 1.0, 0.0, 1.0);
  texCoord = 0.5 * vec2(d) + vec2(0.25);
}

// EOF
