/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Caustics Shader
===================================================================== */

#version 330 core

uniform sampler2D tex;

out vec2 texCoord;

void main(void) {
  int w = textureSize(tex, 0).x;
  ivec2 d = ivec2(0, 0);
  switch (gl_VertexID % 6) {
    case 0: d = ivec2(0, 0); break;
    case 1: d = ivec2(1, 0); break;
    case 2: d = ivec2(1, 1); break;
    case 3: d = ivec2(1, 1); break;
    case 4: d = ivec2(0, 1); break;
    case 5: d = ivec2(0, 0); break;
  }
  int t = gl_VertexID / 6;
  int p = t % w;
  int q = t / w;
  ivec2 tc = ivec2(p, q) + d;
  if (tc.x < 0) { tc.x += w; }
  if (tc.y < 0) { tc.y += w; }
  vec3 c = vec3(vec2(tc), 0.0);
  vec3 s = vec3(vec2(tc), 256.0);
  if (tc.x >= w) { tc.x -= w; }
  if (tc.y >= w) { tc.y -= w; }
  ivec2 tcx1 = tc + ivec2(1, 0);
  if (tcx1.x < 0) { tcx1.x += w; }
  if (tcx1.y < 0) { tcx1.y += w; }
  if (tcx1.x >= w) { tcx1.x -= w; }
  if (tcx1.y >= w) { tcx1.y -= w; }
  ivec2 tcx2 = tc - ivec2(1, 0);
  if (tcx2.x < 0) { tcx2.x += w; }
  if (tcx2.y < 0) { tcx2.y += w; }
  if (tcx2.x >= w) { tcx2.x -= w; }
  if (tcx2.y >= w) { tcx2.y -= w; }
  ivec2 tcy1 = tc + ivec2(0, 1);
  if (tcy1.x < 0) { tcy1.x += w; }
  if (tcy1.y < 0) { tcy1.y += w; }
  if (tcy1.x >= w) { tcy1.x -= w; }
  if (tcy1.y >= w) { tcy1.y -= w; }
  ivec2 tcy2 = tc - ivec2(0, 1);
  if (tcy2.x < 0) { tcy2.x += w; }
  if (tcy2.y < 0) { tcy2.y += w; }
  if (tcy2.x >= w) { tcy2.x -= w; }
  if (tcy2.y >= w) { tcy2.y -= w; }
  float h = texelFetch(tex, tc, 0).r;
  s.z += h;
  float hx1 = texelFetch(tex, tcx1, 0).r;
  float hx2 = texelFetch(tex, tcx2, 0).r;
  float hy1 = texelFetch(tex, tcy1, 0).r;
  float hy2 = texelFetch(tex, tcy2, 0).r;
  vec3 x = normalize(vec3(1.0, 0.0, 0.5 * (hx1 - hx2)));
  vec3 y = normalize(vec3(0.0, 1.0, 0.5 * (hy1 - hy2)));
  vec3 normal = normalize(cross(x, y));
  if (normal.z < 0.0) {
    normal = -normal;
  }
  vec3 incident = normalize(vec3(0.0, 0.0, -1.0));
  float eta = 1.0/1.333;
  vec3 r = refract(incident, normal, eta);
  if (r.z < 0.0) {
    float k = s.z / r.z;
    c = s - k * r;
  }
  gl_Position = vec4(vec2(c.xy) / float(w) * 2.0 - vec2(1.0), 0.0, 2.0);
  texCoord = vec2(d);
}

// EOF
