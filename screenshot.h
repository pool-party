/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Screenshot Module
===================================================================== */

#ifndef SCREENSHOT_H
#define SCREENSHOT_H 1

//======================================================================
// screenshot module data
struct screenshot {
  int width;
  int height;
  char *buffer;
  int rt;
};

//======================================================================
// prototypes
struct screenshot *screenshot_init(struct screenshot *screenshot, int rt);
void screenshot_display(struct screenshot *screenshot);
void screenshot_reshape(struct screenshot *screenshot, int w, int h);

#endif
