/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Wrap Seams Shader
===================================================================== */

#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "wrapseams.h"
#include "wrapseams.vert.c"
#include "wrapseams.frag.c"

//======================================================================
// initialization
struct wrapseams *wrapseams_init(struct wrapseams *wrapseams) {
  if (! wrapseams) { return 0; }
  if (! shader_init(&wrapseams->shader, wrapseams_vert, wrapseams_frag)) {
    return 0;
  }
  shader_uniform(wrapseams, tex);
  wrapseams->value.tex = 0;
  wrapseams->width = 0;
  wrapseams->height = 0;
  glGenTextures(1, &(wrapseams->texture));
  glBindTexture(GL_TEXTURE_2D, wrapseams->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glGenVertexArrays(1, &wrapseams->vao);
  return wrapseams;
}

//======================================================================
// reshape callback
void wrapseams_reshape(
  struct wrapseams *wrapseams, int w, int h
) {
  if (wrapseams->audio[0]) free(wrapseams->audio[0]);
  if (wrapseams->audio[1]) free(wrapseams->audio[1]);
  wrapseams->width  = roundtwo(w);
  wrapseams->height = roundtwo(h);
  float *zero = calloc(1, sizeof(float) * wrapseams->width * wrapseams->height);
  glBindTexture(GL_TEXTURE_2D, wrapseams->texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F,
    wrapseams->width, wrapseams->height,
    0, GL_RED, GL_FLOAT, zero
  );
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  free(zero);
  wrapseams->audio[0] = calloc(1, wrapseams->height * sizeof(float));
  wrapseams->audio[1] = calloc(1, wrapseams->height * sizeof(float));
}

//======================================================================
// display callback
void wrapseams_display(
  struct wrapseams *wrapseams, GLuint fbo, GLuint texture
) {
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
    wrapseams->texture, 0
  );
  glBindTexture(GL_TEXTURE_2D, texture);
  glUseProgram(wrapseams->shader.program);
  shader_updatei(wrapseams, tex);
  glViewport(0, 0, wrapseams->width, wrapseams->height);
  glBindVertexArray(wrapseams->vao);
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glUseProgram(0);
  glBindTexture(GL_TEXTURE_2D, wrapseams->texture);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
    wrapseams->texture, 0
  );
  glReadPixels(31 * wrapseams->width / 64, 0, 1, wrapseams->height, GL_RED, GL_FLOAT, wrapseams->audio[0]);
  glReadPixels(33 * wrapseams->width / 64, 0, 1, wrapseams->height, GL_RED, GL_FLOAT, wrapseams->audio[1]);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//======================================================================
// audio callback
void wrapseams_audio(struct wrapseams *wrapseams, float *left, float *right) {
  memcpy(left,  wrapseams->audio[0], wrapseams->height * sizeof(float));
  memcpy(right, wrapseams->audio[1], wrapseams->height * sizeof(float));
}

// EOF
