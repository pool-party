/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Wrap Seams Shader
===================================================================== */

#ifndef WRAPSEAMS_H
#define WRAPSEAMS_H 1

#include "shader.h"

//======================================================================
// data
struct wrapseams { struct shader shader;
  struct { GLint  tex; } uniform;
  struct { int    tex; } value;
  GLuint texture;
  GLuint vao;
  int width;
  int height;
  float *audio[2];
};

//======================================================================
// protoypes
struct wrapseams *wrapseams_init(struct wrapseams *wrapseams);
void wrapseams_reshape(
  struct wrapseams *wrapseams, int w, int h
);
void wrapseams_display(
  struct wrapseams *wrapseams, GLuint fbo, GLuint texture
);
void wrapseams_audio(struct wrapseams *wrapseams, float *left, float *right);

#endif
