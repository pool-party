/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Main Module
===================================================================== */

#ifndef PARTY_H
#define PARTY_H 1

#include <math.h>
#include <time.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <jack/jack.h>
#include <sndfile.h>

#include "waves.h"
#include "cubic.h"
#include "caustics.h"
#include "wrapseams.h"
#include "pool.h"
#include "screenshot.h"

//======================================================================
// configuration
#define party_tex_size 1024

#define twopi 6.283185307179586
#define SR 48000
#define FPS 30
typedef struct { double y; } HIP;
static inline double hip(HIP *s, double x, double hz) {
  double c = fmin(fmax(1 - twopi * hz / SR, 0), 1);
  double n = 0.5 * (1 + c);
  double y = x + c * s->y;
  double o = n * (y - s->y);
  s->y = y;
  return o;
}
typedef struct { double y; } LOP;
static inline double lop(LOP *s, double x, double hz) {
  double c = fmin(fmax(twopi * hz / SR, 0), 1);
  return s->y = x * c + s->y * (1 - c);
}


//======================================================================
// main module data
struct party {
  struct waves waves;
  struct cubic cubic;
  struct caustics caustics;
  struct wrapseams wrapseams;
  struct pool pool;
  struct screenshot screenshot;
  GLuint fbo;
  time_t starttime;
  unsigned int frame;
  int done;
  int fullscreen;
  char *behaviour;
  float audio[8][1024];
  float center[4];
  int ix;
  LOP lop[2];
  HIP hip[2];
  int rt;
  jack_client_t *client;
  jack_port_t *port[2];
  SNDFILE *sndfile;
};

//======================================================================
// prototypes
int party_init(int rt);
void party_reshape(int w, int h);
void party_display(GLFWwindow *window);
void party_atexit(void);
void party_idle(void);
void party_keynormal(unsigned char key, int x, int y);

#endif
