/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Cubic Interpolation Shader
===================================================================== */

#ifndef CUBIC_H
#define CUBIC_H 1

#include "shader.h"

//======================================================================
// data
struct cubic { struct shader shader;
  struct { GLint  tex, size, delta; } uniform;
  struct { int tex, size[2], delta[2]; } value;
  GLuint textures[2];
  GLuint vao;
  int width;
  int height;
};

//======================================================================
// protoypes
struct cubic *cubic_init(struct cubic *cubic);
void cubic_reshape(
  struct cubic *cubic, int w, int h
);
void cubic_display(
  struct cubic *cubic, GLuint fbo, GLuint texture
);

#endif
