/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Caustics Shader
===================================================================== */

#ifndef CAUSTICS_H
#define CAUSTICS_H 1

#include "shader.h"

//======================================================================
// data
struct caustics { struct shader shader;
  struct { GLint  tex; } uniform;
  struct { int    tex; } value;
  GLuint texture;
  GLuint vao;
  int width;
  int height;
};

//======================================================================
// protoypes
struct caustics *caustics_init(struct caustics *caustics);
void caustics_reshape(
  struct caustics *caustics, int w, int h
);
void caustics_display(
  struct caustics *caustics, GLuint fbo, GLuint texture
);

#endif
