/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Wrap Seams Shader
===================================================================== */

#version 330 core

uniform sampler2D tex;
in vec2 texCoord;
out vec4 colour;

void main(void) {
  colour = vec4
    ( texture(tex, texCoord).r
    + texture(tex, texCoord + vec2(0.5, 0.0)).r
    + texture(tex, texCoord + vec2(0.0, 0.5)).r
    + texture(tex, texCoord + vec2(0.5, 0.5)).r
    );
}

// EOF
