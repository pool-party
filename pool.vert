/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
mightymandel -- GPU-based Mandelbrot Set explorer
Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
------------------------------------------------------------------------
Cubic Interpolation Shader
===================================================================== */

#version 330 core

uniform ivec2 size;

out vec2 p0;

void main(void) {
  vec2 q = vec2(0.0);
  switch (gl_VertexID) {
  case 0: q = vec2(0.0, 0.0); break;
  case 1: q = vec2(1.0, 0.0); break;
  case 2: q = vec2(0.0, 1.0); break;
  case 3: q = vec2(1.0, 1.0); break;
  }
  gl_Position = vec4(2.0 * q - vec2(1.0), 0.0, 1.0);
  p0 = vec2(q.x, mix(0.5 - 9.0 / 32.0, 0.5 + 9.0 / 32.0, q.y));
}

// EOF
