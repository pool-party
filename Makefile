#=======================================================================
# pool-party -- water simulation
# Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
#-----------------------------------------------------------------------
# based on:
# rdex -- reaction-diffusion explorer
# Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
#-----------------------------------------------------------------------
# Makefile
#=======================================================================

CC = gcc
CFLAGS = -std=c99 -Wall -Wextra -pedantic -O3 -fPIC -fopenmp `pkg-config --cflags jack`
LIBS = -lGL -lGLU -lglfw -lGLEW -lfftw3f -lm `pkg-config --libs jack` -lsndfile
OBJS = main.o party.o shader.o caustics.o cubic.o pool.o util.o waves.o screenshot.o wrapseams.o
GENC = caustics.frag.c caustics.vert.c cubic.frag.c cubic.vert.c pool.frag.c pool.vert.c wrapseams.frag.c wrapseams.vert.c

# optional feature: debuggability
CFLAGS += -ggdb

all: pool-party

clean:
	-rm -f $(OBJS) $(GENC)

# no suffix rules, they cause weird issues with the .frag.c files
.SUFFIXES:
.PHONY: all clean

# link program
pool-party: $(OBJS)
	$(CC) $(CFLAGS) -s -o pool-party $(OBJS) $(LIBS)

# C source to object file
%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

# shader source to C source
%.frag.c: %.frag s2c.sh
	./s2c.sh $*_frag < $< > $@
%.vert.c: %.vert s2c.sh
	./s2c.sh $*_vert < $< > $@

# dependencies
caustics.o: caustics.c caustics.frag.c caustics.h caustics.vert.c shader.h util.h
cubic.o: cubic.c cubic.frag.c cubic.vert.c cubic.h shader.h util.h
main.o: main.c party.h
pool.o: pool.c pool.h pool.frag.c pool.vert.c shader.h util.h
party.o: party.c party.h caustics.h cubic.h waves.h pool.h screenshot.h wrapseams.h
screenshot.o: screenshot.c screenshot.h
shader.o: shader.c shader.h
util.o: util.c util.h
waves.o: waves.c waves.h util.h
wrapseams.o: wrapseams.c wrapseams.h wrapseams.vert.c wrapseams.frag.c shader.h util.h

# EOF
