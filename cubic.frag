/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
mightymandel -- GPU-based Mandelbrot Set explorer
Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
------------------------------------------------------------------------
Cubic Interpolation Shader
===================================================================== */

#version 330 core

uniform sampler2D tex;
uniform ivec2 size; // FIXME must be power of two
uniform ivec2 delta; // (1,0) or (0,1)

in vec2 p;

out vec4 colour;

vec4 interp(float t, mat4 a) {
  const mat4 m = mat4
    ( 0.0,2.0,0.0,0.0
    , -1.0,0.0,1.0,0.0
    , 2.0,-5.0,4.0,-1.0
    , -1.0,3.0,-3.0,1.0
    );
  vec4 f = vec4(1.0, t, t*t, t*t*t);
  return 0.5 * a * m * f;
}

void main(void) {
  ivec2 i = ivec2(floor(p));
  ivec2 i0 = (i -     delta) & ivec2(size - ivec2(1));
  ivec2 i1 = (i            ) & ivec2(size - ivec2(1));
  ivec2 i2 = (i +     delta) & ivec2(size - ivec2(1));
  ivec2 i3 = (i + 2 * delta) & ivec2(size - ivec2(1));
  vec2 f = p - vec2(i);
  colour =
    interp(dot(f, vec2(delta)), mat4(
      texelFetch(tex, i0, 0),
      texelFetch(tex, i1, 0),
      texelFetch(tex, i2, 0),
      texelFetch(tex, i3, 0)));
}

// EOF
