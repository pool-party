/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Caustics Shader
===================================================================== */

#version 330 core

in vec2 texCoord;
out vec4 colour;

void main(void) {
  vec2 x = dFdx(texCoord);
  vec2 y = dFdy(texCoord);
  float a = length(vec2(x.x, y.x)) * length(vec2(x.y, y.y));
  colour = vec4(vec3(1.0), a);
}

// EOF
