/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Wave Simulation
===================================================================== */

#ifndef WAVES_H
#define WAVES_H 1

#include <GL/glew.h>
#include <complex.h>
#include <fftw3.h>

//======================================================================
// data
struct waves {
  int hasplan;
  fftwf_plan plan;
  float _Complex *inbuf;
  float _Complex *outbuf;
  float _Complex *h00;
  float *h0;
  float *w;
  float *wp;
  float *texbuf;
  GLuint texture;
  int width;
  int height;
  
  int frame;
};

//======================================================================
// protoypes
struct waves *waves_init(struct waves *waves);
void waves_reshape(
  struct waves *waves, int w, int h
);
void waves_display(
  struct waves *waves
);
void waves_audio(struct waves *waves, float *left, float *right, float *center);
void waves_adjust(struct waves *waves, double wx, double wy);

#endif
