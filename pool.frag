/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008,2009,2010  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Refraction Shader
===================================================================== */

#version 330 core

uniform sampler2D texSurface;
uniform sampler2D texCaustics;

const float pi = 3.141592653589793;

in vec2 p0;

out vec4 colour;

void main(void) {
  int w = textureSize(texCaustics, 0).x;

  vec2 p = p0;
  float u = 2.0 * p.x - 1.0;
  float v = 2.0 * p.y - 1.0;
  u *= 0.75;
  v *= 0.75;
  vec3 incident = normalize(vec3(2.0 * u, 2.0 * v, u * u + v * v - 1.0));

  if (incident.z > 0.0) {
    // do cloud reflection

    discard;

  } else {
    // do water refraction and reflection

    vec3 eye = vec3(0.0, 0.0, 0.5);
    float e = eye.z / incident.z;
    p = vec2(eye - e * incident);
    p += vec2(0.5);
    vec2 dx = vec2(1.0 / float(w), 0.0);
    vec2 dy = vec2(0.0, 1.0 / float(w));
    float h = texture(texSurface, p).r;
    float hx1 = texture(texSurface, p + dx).r;
    float hx2 = texture(texSurface, p - dx).r;
    float hy1 = texture(texSurface, p + dy).r;
    float hy2 = texture(texSurface, p - dy).r;
    vec3 x = normalize(vec3(1.0, 0.0, 0.5 * (hx1 - hx2)));
    vec3 y = normalize(vec3(0.0, 1.0, 0.5 * (hy1 - hy2)));
    vec3 normal = normalize(cross(x, y));
    if (normal.z < 0.0) {
      normal = -normal;
    }

    float caustic = 0.0;
    float eta = 1.0/1.333;
    vec3 r = refract(incident, normal, eta);
    if (dot(normal, incident) < 0.0 && r.z < 0.0) {
      vec3 s = vec3(p * float(w), h + 256.0);
      float k = s.z / r.z;
      vec3 c = s - k * r;
      vec2 q = c.xy / float(w);
      caustic = 0.5 * log(1.0 + texture(texCaustics, q).r);
    }

    float sky = 0.0;
    vec3 f = vec3(0.0);
    if (dot(normal, incident) < 0.0) {
      f = reflect(incident, normal);
      vec3 light = normalize(vec3(0.0, 0.0, 1.0));
      sky = mix(0.5, 1.0, pow(max(0.0, dot(f, light)), 64.0));
    } else {
      sky = 1.0;
    }
    float a = abs(dot(f, normal));
    float b = abs(dot(r, normal));
    float rs = (eta * a - b) / (eta * a + b);
    float rp = (eta * b - a) / (eta * b + a);
    float fresnel = 0.5 * (rs * rs + rp * rp);

    float grey = tanh(clamp(mix(caustic, sky, fresnel), 0.0, 4.0));
    vec3 rgb = vec3(grey) + vec3(0.1, 0.1, 0.2)
      * sin(2.0 * pi * pow(vec3(grey), vec3(0.5, 0.666, 0.75)));
    colour = vec4(rgb, 1.0);
  }
}

// EOF
