/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Refraction Shader
===================================================================== */

#include "util.h"
#include "pool.h"
#include "pool.vert.c"
#include "pool.frag.c"

//======================================================================
// initialization
struct pool *pool_init(struct pool *pool) {
  if (! pool) { return 0; }
  if (! shader_init(&pool->shader, pool_vert, pool_frag)) {
    return 0;
  }
  shader_uniform(pool, texSurface);
  shader_uniform(pool, texCaustics);
  pool->value.texSurface = 1;
  pool->value.texCaustics = 0;
  pool->width = 0;
  pool->height = 0;
  glGenVertexArrays(1, &pool->vao);
  return pool;
}

//======================================================================
// reshape callback
void pool_reshape(
  struct pool *pool, int w, int h
) {
  pool->width  = w;
  pool->height = h;
}

//======================================================================
// display callback
void pool_display(
  struct pool *pool, GLuint texSurface, GLuint texCaustics
) {
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, texSurface);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texCaustics);
  glUseProgram(pool->shader.program);
  shader_updatei(pool, texSurface);
  shader_updatei(pool, texCaustics);
  glViewport(0, 0, pool->width, pool->height);
  glClear(GL_COLOR_BUFFER_BIT);
  glBindVertexArray(pool->vao);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(0);
  glUseProgram(0);
}

// EOF
