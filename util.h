/* =====================================================================
pool-party -- water simulation
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
based on:
rdex -- reaction-diffusion explorer
Copyright (C) 2008  Claude Heiland-Allen <claude@mathr.co.uk>
------------------------------------------------------------------------
Utility Functions
===================================================================== */

#ifndef UTIL_H
#define UTIL_H 1

unsigned int roundtwo(unsigned int x);
unsigned int logtwo(unsigned int x);

#endif
